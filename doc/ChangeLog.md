
## Change Log

2023-09-26

Multiple changes have been made in the past couple of weeks. The most important ones are listed here.

* Renamed `vmm-unit-tests` to `vmm-tests`
* Renamed `VMM-test` (the neural network tests) to `mnist-tests`

* Parent project `ACoreTests` was rebased with new version
    * Fixes a bug where `make help` in the `ACoreTests` directory didn't work
    * Now you need to run `make clean-vmm-tests && make` instead of just `make` after changes in source code

* Parent project `a-core_thesydekick` now points to modified version of VMM python model
    * Input and weight addresses now match the physical VMM
    * Input now accepts also negative values (but they don't have to be used if not needed)
    * Output was changed to only include a total of six bits, with configurable output range
    * Note that this breaks compatibility with `mnist-tests` for now

* Changed old define name `#define OUTPUT_ADC_READY` to match datasheet `#define VMM_READY_SRAM_DOUT`
* The `vmm_utils.h` file has been cleaned up and commented
* Old unnecessary files have been removed


