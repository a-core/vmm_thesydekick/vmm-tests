DEFAULT_MAKEFILE_PATH ?= ../a-core-library

MARCH ?= rv32im
MABI ?= ilp32

A_CORE_LIB_PATH ?= ../a-core-library
A_CORE_HEADERS_PATH ?= ../../../ACoreChip/chisel/include

include $(DEFAULT_MAKEFILE_PATH)/default_c.mk

VMM_UTILS_PATH = ./vmm-utils

INCLUDES := $(INCLUDES) -Iinclude -I$(VMM_UTILS_PATH)/include -DACORE
CSOURCES := $(CSOURCES) $(wildcard src/*.c) $(wildcard $(VMM_UTILS_PATH)/src/*.c)
LDOPTS := $(LDOPTS) -lm
CFLAGS := $(CFLAGS) -Wall


