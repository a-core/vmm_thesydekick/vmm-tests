
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include "a-core.h"
#include "a-core-utils.h"
#include "acore-uart.h"
#include "acore-gpio.h"
#include "vmm_utils.h"



int main() {

	printf("\nSTART THE TEST\n\n");



	// Declare and initialize a vector with binary values
    	//uint8_t vector[2] = {0b00010101, 0b00011100}; // 



	// Declare and initialize a 2D matrix with binary values
	//uint8_t matrix_weight[2] = { 0b01011100, 0b01100011}; // 

				    // positive number denote by 01-6bits


				// positive 01 for filter weights 
				// negative 10 for filter weights 


	// write vector inputs
	for (int i = 0; i < 36; i++) {
		vmm_write_vec_in_bin(i, 0b00111111); // in 1x36 vector input we are placing all 36 values as 1 = 111111 values 
	}



	// write matrix inputs
	for (int j = 0; j < 36 ; j++){
		vmm_write_mat_in_bin(j, 0, 0b01111111); // 

	}



	// Read and display the single output vector element
	uint8_t output = vmm_read_vec_out_bin(0);  // Reading from column 0, assuming single output

	printf("Output: 0x%02X\n", output);

	vmm_write_sram_rd(1);

	printf("\nEND THE TEST\n\n");
	test_pass();
	test_end();

}
