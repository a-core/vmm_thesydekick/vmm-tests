#include <stdio.h>

#include "test_axi4l_regs.h"
#include "vmm_utils.h"
#include "a-core-utils.h"


bool test_axi4l_regs1() {
    printf("test_axi4l_regs1:\n");
    bool ret = true;

    // Set r_tune to 0, so we can assume that everything starts from 0.
    // Also check that we didn't alter anything else.
    if (vmm_read_r_tune() != 1) {
        printf("\tERR: r_tune initial value differs\n");
    }
    uint32_t val;
    val = gpi_read((volatile uint32_t *)VMM_RESET_CONTROL);
    vmm_write_r_tune(0);
    if ((gpi_read((volatile uint32_t *)VMM_RESET_CONTROL) != (val & (~(1UL << 20)))) || (vmm_read_r_tune() != 0)) {
        printf("\tERR: Manually initializing r_tune to 0 failed\n");
    }

    typedef struct {
        char name[20];
        bool (*func_read)();
        void (*func_write)(bool);
        volatile uint32_t * offset_from;
        unsigned offset;
        uint32_t word_initial_value;
    } VmmAxiBit;

    volatile uint32_t * vrc = (volatile uint32_t *)VMM_RESET_CONTROL;
    volatile uint32_t * sdca = (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR;
    uint32_t vrc_init_val = gpi_read(vrc);
    uint32_t sdca_init_val = gpi_read(sdca);
    VmmAxiBit bits[] = {
        { "r_tune",             vmm_read_r_tune,           vmm_write_r_tune,          vrc,  20, vrc_init_val },
        { "comp_en",            vmm_read_comp_en,          vmm_write_comp_en,         vrc,  19, vrc_init_val },
        { "comp_rdb_wr_gbl",    vmm_read_comp_rdb_wr_gbl,  vmm_write_comp_rdb_wr_gbl, vrc,  18, vrc_init_val },
        { "lsb_tune",           vmm_read_lsb_tune,         vmm_write_lsb_tune,        vrc,  17, vrc_init_val },
        { "reset_ip_comp",      vmm_read_reset_ip_comp,    vmm_write_reset_ip_comp,   vrc,  6 , vrc_init_val },
        { "reset_op_comp",      vmm_read_reset_op_comp,    vmm_write_reset_op_comp,   vrc,  5 , vrc_init_val },
        { "reset_sram",         vmm_read_reset_sram,       vmm_write_reset_sram,      vrc,  4 , vrc_init_val },
        { "reset_adc",          vmm_read_reset_adc,        vmm_write_reset_adc,       vrc,  3 , vrc_init_val },
        { "sh_en",              vmm_read_sh_en,            vmm_write_sh_en,           vrc,  2 , vrc_init_val },
        { "vcm_en",             vmm_read_vcm_en,           vmm_write_vcm_en,          vrc,  1 , vrc_init_val },
        { "adc_mux_wr",         vmm_read_adc_mux_wr,       vmm_write_adc_mux_wr,      vrc,  0 , vrc_init_val },

        { "sram_wr",            vmm_read_sram_wr,          vmm_write_sram_wr,         sdca, 14 , sdca_init_val },
        { "sram_rd",            vmm_read_sram_rd,          vmm_write_sram_rd,         sdca, 13 , sdca_init_val },
    };

    // Check initial values (r_tune (bits[0]) already checked earlier)
    for (int i = 1; i < sizeof(bits) / sizeof(bits[0]); i++) {
        if (bits[i].func_read() != 0) {
            printf("\tERR: %s initial value differs\n", bits[i].name);
            ret = false;
        }
    }
    // Check write / read
    for (int i = 0; i < sizeof(bits) / sizeof(bits[0]); i++) {
        // Check writing 1
        bits[i].func_write(1);
        val = gpi_read((volatile uint32_t *)(bits[i].offset_from));
        if (val != (bits[i].word_initial_value | (1UL << bits[i].offset))) {
            printf("\tERR: Writing %s to 1 failed\n", bits[i].name);
            ret = false;
        }
        // Check reading 1
        if (bits[i].func_read() != 1) {
            printf("\tERR: Reading value 1 from %s failed\n", bits[i].name);
            ret = false;
        }
        // Check writing 0
        bits[i].func_write(0);
        val = gpi_read((volatile uint32_t *)(bits[i].offset_from));
        if (val != bits[i].word_initial_value) {
            printf("\tERR: Writing %s to 0 failed\n", bits[i].name);
            ret = false;
        }
        // check reading 0
        if (bits[i].func_read() != 0) {
            printf("\tERR: Reading value 0 from %s failed\n", bits[i].name);
            ret = false;
        }
    }
    if (ret) {
        printf("\tOK\n");
        return true;
    } else {
        // ERR already printed earlier in this test if one occured
        return false;
    }
}

bool test_axi4l_regs2() {
    printf("test_axi4l_regs2:\n");
    bool ret = true;

    // Set r3_tune to all 0's, so we can assume that everything starts from 0.
    // Also check that we didn't alter anything else.
    if (vmm_read_r3_tune() != 0x100) {
        printf("\tERR: r3_tune initial value differs\n");
    }
    uint32_t val;
    val = gpi_read((volatile uint32_t *)VMM_RESET_CONTROL);
    vmm_write_r3_tune(0x00);
    if ((gpi_read((volatile uint32_t *)VMM_RESET_CONTROL) != (val & (~((512-1) << 8)))) || (vmm_read_r3_tune() != 0x00)) {
        printf("\tERR: Manually initializing r_tune to 0 failed\n");
    }

    typedef struct {
        char name[20];
        uint32_t (*func_read)();
        void (*func_write)(uint32_t);
        uint32_t initial_value;
        volatile uint32_t * offset_from;
        unsigned offset;
        uint32_t mask;
    } VmmAxiBit;

    volatile uint32_t * vrc = (volatile uint32_t *)VMM_RESET_CONTROL;
    volatile uint32_t * sdca = (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR;
    uint32_t vrc_init_val = gpi_read(vrc);
    uint32_t sdca_init_val = gpi_read(sdca);
    VmmAxiBit bits[] = {
        { "r3_tune",            vmm_read_r3_tune,           vmm_write_r3_tune,        vrc_init_val,  vrc,  8,  (512-1) << 8 },
        { "adc_mux_config",     vmm_read_adc_mux_config,    vmm_write_adc_mux_config, vrc_init_val,  vrc,  24, (32-1) << 24 },

        { "sram_addr",          vmm_read_sram_addr,         vmm_write_sram_addr,      sdca_init_val, sdca, 0,  (4096-1) << 0 },
        { "sram_din",           vmm_read_sram_din,          vmm_write_sram_din,       sdca_init_val, sdca, 16, (256-1) << 16 },
    };

    // Check initial values (r3_tune (bits[0]) already checked earlier)
    for (int i = 1; i < sizeof(bits) / sizeof(bits[0]); i++) {
        if ((bits[i].initial_value & bits[i].mask) != 0x00) {
            printf("\tERR: %s initial value differs\n", bits[i].name);
        }
    }
    // Check write / read
    for (int i = 0; i < sizeof(bits) / sizeof(bits[0]); i++) {
        // Check writing all 1's
        bits[i].func_write(0xffffffff);
        val = gpi_read((volatile uint32_t *)(bits[i].offset_from));
        if (val != (bits[i].initial_value | bits[i].mask)) {
            printf("\tERR: Writing %s to all 1's failed\n", bits[i].name);
            ret = false;
        }
        // Check reading all 1's
        if (bits[i].func_read() != ((0xffffffff & bits[i].mask) >> bits[i].offset)) {
            printf("\tERR: Reading value \"all 1's\" from %s failed\n", bits[i].name);
            ret = false;
        }
        // Check writing all 0's
        bits[i].func_write(0x00);
        val = gpi_read((volatile uint32_t *)(bits[i].offset_from));
        if (val != bits[i].initial_value) {
            printf("\tERR: Writing %s to all 0's failed\n", bits[i].name);
            ret = false;
        }
        // check reading all 0's
        if (bits[i].func_read() != 0x00) {
            printf("\tERR: Reading value \"all 0's\" from %s failed\n", bits[i].name);
            ret = false;
        }
    }
    if (ret) {
        printf("\tOK\n");
        return true;
    } else {
        // ERR already printed earlier in this test if one occured
        return false;
    }
}


