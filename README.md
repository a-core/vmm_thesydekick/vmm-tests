This repository hosts VMM (Vector-Matrix Multiplier) unit test project originally developed by Mikael Mieskonen.

## Tutorial on how to run simulations with A-Core+VMM combo

### 0. Cloning the project

```shell
eval `ssh-agent -c` && ssh-add
git clone git@gitlab.com:a-core/vmm_thesydekick/a-core_thesydekick.git
cd a-core_thesydekick
./init_submodules.sh
./configure
./pip3userinstall.sh
```

A short explanation of commands:

* The A-Core project includes multiple different smaller projects as git submodules. The init_submodules.sh script
  will clone and initialize all the submodules.
* Cloning many different projects will ask your ssh-key passphrase multiple times during the process, unless ssh-agent
  is used at the start
* The pip3userinstall.sh script installs required python modules


### 1. Simulation environment

The A-Core project uses [TheSyDeKick framework](https://github.com/TheSystemDevelopmentKit) to structure
everything inside the project.

Inside the cloned `a-core_thesydekick` directory you will find a directory structure something like this
(I have only listed the most important parts here):

```
.
├── init_submodules.sh
├── configure
├── pip3userinstall.sh
├── sourceme.csh
└── Entities
    ├── ACoreChip
    ├── vmm_top
    ├── ADCmodule
    ├── DACmodule
    ├── ACoreTestbenches
    └── ACoreTests
        ├── ACoreTests
        ├── configure
        └── sw
            └── vmm-tests
```

The initialization scripts ran at the previous step are located in the top directory. There is also a subdirectory
called `Entities` which includes all of the subprojects as git submodules.

* `ACoreChip` includes scala model of A-Core
* `vmm_top`, `ADCmodule` and `DACmodule` include python models of VMM, ADC and DAC
* ACoreTestbenches includes different testbenches for different tests. For example there is one testbench used for
  tests with VMM. That testbench connects A-Core and VMM together in the same testbench.
* ACoreTests directory includes C and assembly tests that can be simulated with A-Core. Under the sw subdirectory
  you can see for example `vmm-tests` which I will be using as an
  example for this tutorial.

TheSyDeKick framework demands that all of the entities (submodules that are located under Entities direcotry) have
the same structure. [This](https://thesystemdevelopmentkit.github.io/docs/introduction.html#naming-and-structure)
part of the sydekick documentation explains the structure for each entity.

The top module and submodules also contain README's which might be helpful.


### 2. Running the simulation

I'm using `vmm-tests` as an example for running C tests that simulate A-Core the VMM python model. This is
a small test I created and all the other tests can be run in a similar fashion.

Run the simulation:

```shell
source sourceme.csh (only when using Aalto ECD group computers)
cd Entities/ACoreTests
./configure
make test_config=vmm-tests simulator=cocotb sim_backend=verilator
```

If you make changes to source code you need to `make clean-<test_config name>` before running make again. In
this case:

```shell
make clean-vmm-tests
make test_config=vmm-tests simulator=cocotb sim_backend=verilator
```

A short explanation of commands:

* Sourcing sourceme.csh adds needed programs to path. This only need to be run if a new shell is started since
  the last run
* In the sydekick environment you always run tests using `./configure && make`
* ACoreTests directory contains multiple different tests for A-Core. You can choose which one you want to
  run with the `test_config` parameter
* In VMM tests `cocotb` must be used as `simulator` because the python model of VMM uses cocotb python library
* `verilator` is a recent addition for `sim_backend` and will make the simulation run faster
* More details about running simulations can be found from the ACoreTests
  [README](https://gitlab.com/a-core/vmm_thesydekick/acoretests/-/tree/master?ref_type=heads) file


### 3. Closer look at different parts of the environment

#### 3.1 Python models of VMM, DAC and ADC

The most relevant parts of these models are found from:
* `Entities/vmm_top/vmm_top/__init__.py`
* `Entities/DACmodule/DACmodule/__init__.py`
* `Entities/ADCmodule/ADCmodule/__init__.py`

#### 3.2 Simulation testbench

`Entities/ACoreTestbenches/ACoreTestbenches/test_acore_vmm.py` includes the testbench for ACore+VMM tests, where
the A-Core model is connected with VMM python model.

#### 3.3 The C test

Here is a simplified version of the `vmm-tests` directory:

```
.
├── a-core.ld
├── crt0.s
├── Makefile
├── README.md
├── include
│   └── test_axi4l_regs.h
└── src
    ├── test_axi4l_regs.c
    └── main.c
```

* test_axi4l_regs.h includes tests for AXI4L registers
* main.c runs tests found from test_axi4l_regs

You can also find A-Core specific functions defined in
`Entities/ACoreTests/sw/a-core-library/a-core-utils/include/a-core-utils.h`.


